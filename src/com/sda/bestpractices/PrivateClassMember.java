package com.sda.bestpractices;

import lombok.Data;

import java.time.LocalDate;

@Data
public class PrivateClassMember {
    private String name;
    private String surname;
    private Integer idNumber;
    private LocalDate birthDate;



}
