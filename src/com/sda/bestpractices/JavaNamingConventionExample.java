package com.sda.bestpractices;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class JavaNamingConventionExample {

    public final static int AGE_LIMIT = 10;
    public final static double PI = 3.14;

    //private String str; // not recommended to use, not readable variable name..-> (this is used for name)
    private String name;
    private Date birthDate;
    //private LocalDateTime xprDt; not readable
    private LocalDateTime expirationDate;
    //private int x;
    private int counter;

    private static int calculateAge(final Date birthDate) {
        return LocalDate.now().getYear() - birthDate.getYear();
    }
}
